from app import db
from app.models import Image, Kw, Article
import os
import numpy as np
import jieba


root_dir = "app/static/pic/"
sorts = os.listdir(root_dir)


def num_db(local_dir, num, assort, dic):
    local_dir = local_dir + num + "/"
    pages = os.listdir(local_dir)
    pages.sort()
    title = ""
    if os.path.exists(local_dir + "description"):
        with open(local_dir + "description", "r") as f:
            title = f.readline()
            title = title.split("_")[0]
    art = Article(assort=assort, num=num, title=title)
    seg_list = jieba.cut(title)
    seg_list = set(seg_list)
    for i in seg_list:
        if len(i)<2 or dic[i]<2:
            continue
        else:
            keyword = Kw.query.filter_by(word=i).first()
            if keyword is None:
                keyword = Kw(word=i)
                db.session.add(keyword)
                db.session.commit()
            keyword.articles.append(art)
    print(num)
    for page in pages:
        if page != "description":
            img = Image(page=page)
            #print(img.url())
            art.pages.append(img)
    db.session.add(art)
    db.session.commit()
    

def sort_db(assort, dic):
    local_dir = root_dir+assort+"/"
    print(local_dir)
    nums = os.listdir(local_dir)
    for num in nums:
        num_db(local_dir, num, assort, dic)


dic = np.load("key_dict.npy", allow_pickle=True).item()
#for assort in sorts:
#   sort_db(assort, dic)


num_db(root_dir+"zhongguo"+"/", "2554", "zhongguo", dic)  # temporary
