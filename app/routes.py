from flask import render_template, request, send_from_directory
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine, cast, Integer, func
import numpy as np
from app import app
from app.models import Image, Article, Kw
import os


@app.route('/')
@app.route('/index')
def index():
    engine = create_engine('sqlite:///app.db')
    Session = sessionmaker(bind=engine)
    session = Session()
    zhongguo = session.query(Article).filter_by(assort="zhongguo", num="2469").limit(1)
    oumei = session.query(Article).filter_by(assort="oumei", num="2172").limit(1)
    rihan = session.query(Article).filter_by(assort="rihan", num="33").limit(1)
    zipai = session.query(Article).filter_by(assort="zipai", num="6").limit(1)
    meinv = session.query(Article).filter_by(assort="meinv", num="115").limit(1)
    content = [zhongguo, oumei, meinv, rihan, zipai]
    d = {0: "中国人体艺术", 1: "日韩人体艺术", 2: "欧美人体艺术", 4: "大胆人体艺术", 3: "张筱雨人体艺术"}
    ass = np.random.choice(["zhongguo", "oumei", "rihan", "zipai", "meinv"])
    art_num = Article.query.filter_by(assort=ass).count()
    art_index = np.random.randint(art_num)
    random_page = Article.query.filter_by(assort=ass, num=f"{art_index}").first()
    return render_template('index.html', title="主页", content=content, d=d, random_page=random_page)


@app.route('/content/<assort>')
def pic(assort):
    page = request.args.get('page', 1, type=int)
    num = request.args.get('num', 1, type=int)
    article_id = Article.query.filter_by(assort=assort, num=f"{num}").first().id
    result = Image.query.filter_by(article_id=article_id)
    pag = result.order_by("page_num").paginate(page, 1, False)
    title = pag.items[0].title
    prev = Article.query.filter_by(assort=f"{assort}", num=f"{num-1}").first()
    next = Article.query.filter_by(assort=f"{assort}", num=f"{num+1}").first()
    ass = np.random.choice(["zhongguo", "oumei", "rihan", "zipai", "meinv"])
    art_num = Article.query.filter_by(assort=ass).count()
    art_index = np.random.randint(art_num)
    random_page = Article.query.filter_by(assort=ass, num=f"{art_index}").first()
    return render_template("page.html", title=title, pag=pag, prev=prev, next=next, random_page=random_page)


@app.route('/<assort>', methods=['GET', 'POST'])
def assort(assort):
    d = {"zhongguo": "中国人体艺术", "rihan": "日韩人体艺术", "oumei": "欧美人体艺术", "meinv": "大胆人体艺术", "zipai": "张筱雨人体艺术"}
    if assort in d.keys():
        page = request.args.get('page', 1, type=int)
        pag = Article.query.filter_by(assort=f"{assort}").order_by(cast(Article.num, Integer)).paginate(page, 10, False)
        ass = np.random.choice(["zhongguo", "oumei", "rihan", "zipai", "meinv"])
        art_num = Article.query.filter_by(assort=ass).count()
        art_index = np.random.randint(art_num)
        random_page = Article.query.filter_by(assort=ass, num=f"{art_index}").first()
        return render_template("assort.html", p_list=pag, assort=assort, title=d[assort], random_page=random_page)
    else:
        return render_template("404.html")


@app.route('/keyword/<kw>')
def keyword(kw):
    result = Kw.query.filter_by(word=kw).first()
    ass = np.random.choice(["zhongguo", "oumei", "rihan", "zipai", "meinv"])
    art_num = Article.query.filter_by(assort=ass).count()
    art_index = np.random.randint(art_num)
    random_page = Article.query.filter_by(assort=ass, num=f"{art_index}").first()
    if result is not None:
        page = request.args.get('page', 1, type=int)
        pag = result.articles.paginate(page, 10, False)
        return render_template("keyword.html", pag=pag, title="\""+kw+"\"", random_page=random_page)
    else:
        return render_template("404.html", random_page=random_page)


@app.route('/tags')
def tags():
    tags = Kw.query.all()
    ass = np.random.choice(["zhongguo", "oumei", "rihan", "zipai", "meinv"])
    art_num = Article.query.filter_by(assort=ass).count()
    art_index = np.random.randint(art_num)
    random_page = Article.query.filter_by(assort=ass, num=f"{art_index}").first()
    return render_template("tag.html", tags=tags, random_page=random_page)


@app.route('/downloads')
def download():
    hentai_url = ""
    ecchi_url = ""
    for item in os.listdir("app/static/zips/"):
        print(item)
        if "hentai" in item:
            hentai_url = item
        if "ecchi" in item:
            ecchi_url = item
    return render_template("new_base.html", ecchi_url=ecchi_url, hentai_url=hentai_url)


@app.route("/download/<path:filename>", methods=['GET'])
def downloader(filename):
    dirpath = os.getcwd()+"/app/static/zips"
    print(os.path.join(dirpath, filename))
    if request.method=="GET":
        if os.path.exists(os.path.join(dirpath, filename)):
            #print("OK")
            return send_from_directory(dirpath,filename,as_attachment=True)
        return render_template("404.html")
