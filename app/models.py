import os
import re
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from app import db


engine = create_engine('sqlite:///app.db')
Base = declarative_base()
pattern = re.compile(r"\d+")


has_keywords = db.Table("has_keywords",
                    db.Column("article_id", db.Integer, db.ForeignKey("article.id"), primary_key=True),
                    db.Column("kw_id", db.Integer, db.ForeignKey("kw.id"), primary_key=True))


class Article(db.Model):
    __tablename__ = "article"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    assort = db.Column(db.String(16))
    num = db.Column(db.String(60))
    title = db.Column(db.String(120), index=True)
    pages = db.relationship("Image", backref=db.backref("article"))

    def __init__(self, assort, num, title):
        self.assort = assort
        self.num = num
        self.title = title

    def __repr__(self):
        return "<Article {}>".format(self.title)


class Image(db.Model):
    __tablename__ = "image"
    id = db.Column(db.Integer, primary_key=True)
    page = db.Column(db.String(60))
    page_num = db.Column(db.Integer)
    article_id = db.Column(db.Integer, db.ForeignKey('article.id'))

    def __init__(self, page):
        self.page = page
        self.page_num = self.get_page_num()

    @property
    def assort(self):
        return Article.query.filter_by(id=self.article_id).first().assort

    @property
    def title(self):
        return Article.query.filter_by(id=self.article_id).first().title

    @property
    def num(self):
        return Article.query.filter_by(id=self.article_id).first().num

    def __repr__(self):
        return "<Image {}/{}>".format(self.title, self.page)

    def url(self):
        return f"static/pic/{self.assort}/{self.num}/{self.page}"

    def href(self):
        return f"/content/{self.assort}?num={self.num}&page={self.page_num}"

    def get_page_num(self):
        return int(re.findall(pattern, self.page)[0])


class Kw(db.Model):
    __tablename__ = "kw"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    word = db.Column(db.String(10), nullable=False)
    articles = db.relationship('Article', lazy='dynamic', secondary=has_keywords, backref=db.backref('keywords', lazy='dynamic'))

    def __repr__(self):
        return "<Keyword {}>".format(self.word)

