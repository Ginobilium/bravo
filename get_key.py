import os
import jieba
import numpy as np


def num_key(local_dir, num, assort, key_dict):
    local_dir = local_dir + num + "/"
    title = ""
    if os.path.exists(local_dir + "description"):
        with open(local_dir + "description", "r") as f:
            title = f.readline()
            title = title.split("_")[0]
    if title != "":
        seg_list = jieba.cut(title)
        for i in seg_list:
            if len(i)<2:
                continue
            elif i not in key_dict.keys():
                key_dict[i] = 1
            else:
                 key_dict[i] += 1



def sort_key(assort, key_dict):
    root_dir = "app/static/pic/"
    local_dir = root_dir+assort+"/"
    nums = os.listdir(local_dir)
    for num in nums:
            num_key(local_dir, num, assort, key_dict)


root_dir = "app/static/pic/"
key_dict = {}
sorts = os.listdir(root_dir)
for assort in sorts:
    sort_key(assort, key_dict)
np.save("key_dict.npy", key_dict)
